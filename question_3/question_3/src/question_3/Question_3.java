package question_3;

import java.util.Scanner;

public class Question_3 {

	public static void main(String[] args) {
        reverseName();
	}

	private static void reverseName() {
		Scanner scanner = new Scanner(System.in);
       
        String firstNameInput;
        String lastNameInput;
        String fullName;
        String fullNameReversed = "";
        
        System.out.println("Enter the First name in the console and press <ENTER>");
        firstNameInput = scanner.nextLine();
        System.out.println("Now enter the Last name in the console and press <ENTER>");
        lastNameInput = scanner.nextLine();
        scanner.close();
        
        fullName = firstNameInput.trim() + " " + lastNameInput.trim();
        System.out.println("First and last name: " + fullName);
        
        for(int i = fullName.length() - 1; i >= 0; i--) {
        	fullNameReversed = fullNameReversed + fullName.charAt(i);
        }
             
        System.out.println("Name reversed: " + fullNameReversed);
	}
}
