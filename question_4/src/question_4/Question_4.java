package question_4;

import java.util.Scanner;

public class Question_4 {

	public static void main(String[] args) {
		determineTriangleType(); 
	}

	private static void determineTriangleType() {
		Scanner scanner = new Scanner(System.in);
        
		int a; int b; int c;
		
		System.out.println("Enter the value of the first side (A) of the triangle. Only numbers are allowed.");
		while (!scanner.hasNextInt()) scanner.next();
		a = scanner.nextInt();
        
		System.out.println("Enter the value of the second side (B) of the triangle. Only numbers are allowed.");
		while (!scanner.hasNextInt()) scanner.next();
		b = scanner.nextInt();
        		
		System.out.println("Enter the value of the third side (C) of the triangle. Only numbers are allowed.");
		while (!scanner.hasNextInt()) scanner.next();
		c = scanner.nextInt();
		
		scanner.close();
		
		// equilateral triangle: all sides equal 
	    if (a == b && b == c ) 
	        System.out.println("\nThese values will form an EQUILATERAL triangle: all sides are equal"); 
	  
	    // isoceles triangle: two sides equal 
	    else if (a == b || a == c || b == c ) 
	        System.out.println("\nThese values will form an ISOCELES triangle: two sides are equal"); 
	  
	    // scalene triangle: all sides different 
	    else
	        System.out.println("\nThese values will form a SCALENE triangle: all sides are different");
	}
}
