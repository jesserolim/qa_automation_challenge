package Cocus.question;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.json.JSONArray;
import org.json.JSONObject;

import com.jayway.jsonpath.JsonPath;

public class JSONUtil {
	
	private static String employeeDataFile = "src/main/resources/employee_data.json";
	
    public static void retrieveData(InputEnum jsonFileType) {

        JSONObject jsonObject = JSONUtil.parseJSONFile(getJsonFilePath(jsonFileType));
    	JSONObject employeeData = JSONUtil.parseJSONFile(employeeDataFile);    	
    	
        for (String key : jsonObject.keySet()) {
        	
        	if (jsonObject.get(key) instanceof JSONArray && key.equals("paths")) {
            	String delimiter = jsonObject.get("delimiter").toString();
        		int arrayLenght= jsonObject.getJSONArray(key).length();
    			JSONArray valuesLevelOne = jsonObject.getJSONArray(key);
    			
    			// outputs two values separated by underscore
        		if (arrayLenght != 1) {
    				outputTwoValues(employeeData, delimiter, valuesLevelOne);
    			
    			//outputs one value or its alternative
    			} else  { 
        			outputValueOrAlternative(employeeData, valuesLevelOne);
        		}
        		
        	// outputs a single value	
            } else if(key.equals("paths")) {
            	outputSingleValue(jsonObject, employeeData, key);
            }
        }       
	}

	private static void outputSingleValue(JSONObject jsonObject, JSONObject employeeData, String key) {
		System.out.println(
				new StringBuilder()
				.append("\nThe value for the path ")
				.append(jsonObject.get(key))
				.append(" is:\n").toString() +
				
				JsonPath.read(employeeData.toString(), "$." + jsonObject.get(key)));
	}

	private static void outputValueOrAlternative(JSONObject employeeData, JSONArray valuesLevelOne) {
		JSONArray valuesLevelTwo = valuesLevelOne.getJSONArray(0);
		
		 Object firstValue = JsonPath.read(
				employeeData.toString(), "$." + valuesLevelTwo.get(0));
		if (!firstValue.toString().equals("[]")){
			System.out.println(
					new StringBuilder()
						.append("\nThe value for the path ")
						.append(valuesLevelTwo.get(0))
						.append(" is: \n").toString() + 
					
					firstValue); 				
		} else {
			System.out.println(
					new StringBuilder()
						.append("\nThere are no values in ")
						.append(valuesLevelTwo.get(0))
						.append(". \nListing values for ")
						.append(valuesLevelTwo.get(1))
						.append(" instead.\n").toString() +
					
					JsonPath.read(
					employeeData.toString(), "$." + valuesLevelTwo.get(1)));
		}
	}

	private static void outputTwoValues(JSONObject employeeData, String delimiter, JSONArray valuesLevelOne) {
		System.out.println(
				new StringBuilder()
					.append("\nThe values for the paths ")
					.append(valuesLevelOne.get(0))
					.append(" and ")
					.append(valuesLevelOne.get(1))
					.append(" are:\n").toString() +
				
				JsonPath.read(
						employeeData.toString(), "$." + valuesLevelOne.get(0))
						+ delimiter + JsonPath.read(
					    employeeData.toString(), "$." + valuesLevelOne.get(1)));
	}

	private static String getJsonFilePath(InputEnum jsonFileType) {
		String filePath = null;
		
        switch(jsonFileType) {
        case SingleValue:
        	filePath = "src/main/resources/single_value.json"; 
        	break;
        case TwoValues:
        	filePath = "src/main/resources/two_values.json";
        	break;
        case AlternativeValue:
        	filePath = "src/main/resources/alternative_value.json";
        	break;
        default:
        	try {
				throw new Exception("Json Type not supported.");
			} catch (Exception e) {
				e.printStackTrace();
			}
        };
     
		return filePath;
	}
      
    public static JSONObject parseJSONFile(String filename) {
        String content = null;
		try {
			content = new String(Files.readAllBytes(Paths.get(filename)));
		} catch (IOException e) {
			e.printStackTrace();
		}
        return new JSONObject(content);        
    }
    
	public enum InputEnum {
		SingleValue, 
		TwoValues, 
		AlternativeValue,
	}   
}


