package Cocus.question;

import java.io.IOException;
import java.util.Scanner;

import Cocus.question.JSONUtil.InputEnum;

public class App 
{
    public static void main(String[] args ) throws IOException
    {
		Scanner scanner = new Scanner(System.in);
		int jsonType;
		
		System.out.println("Enter numbers 1, 2 or 3 to select the json input type you would like to use.\n"
				+ "Please refer to the files in src/main/resources if you want to change any values. \n\n"
				+ "1 (Single value json)\n"
				+ "2 (Two values json with delimiter)\n"
				+ "3 (Two values - second will be used if first is empty)");
		
		while (!scanner.hasNextInt()) scanner.next();
		jsonType = scanner.nextInt();
        scanner.close();
		
        retrieveData(jsonType);
    }

	private static void retrieveData(int jsonType) {
		InputEnum jsonDataFile = null;
		
		switch(jsonType) {
        case 1:
        	jsonDataFile = JSONUtil.InputEnum.SingleValue; 
        	break;
        case 2:
        	jsonDataFile = JSONUtil.InputEnum.TwoValues;
        	break;
        case 3:
        	jsonDataFile = JSONUtil.InputEnum.AlternativeValue;
        	break;
        default:
        	try {
				throw new Exception("Json Type not supported.");
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    	 
    	JSONUtil.retrieveData(jsonDataFile);
	}
}
