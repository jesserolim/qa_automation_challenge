class MyClass:
    def __init__(self): 
        self._my_dict = {"a":123, "b": True} 
        
    def set_key(self, key, value): 
        self._my_dict[key] = value 

    def get_key(self, key): 
        return self._my_dict[key] 
        
    def get_dict(self):
        return self._my_dict

    def get_dict_with_twice_a(self): 
        buffer = self._my_dict["a"]
        buffer *= 2 
        return buffer

test = MyClass()
test.set_key("c", "value for C")
print (test.get_dict_with_twice_a())
print (test.get_key("c"))
print (test.get_dict())