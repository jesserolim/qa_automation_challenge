1) Is there anything wrong with the Java code below? If so, can you elaborate  exactly what and why? Can you propose a fix for it? 
Purpose of this assignment is to warm you up and test your bug hunting skills on  very simple case. 
==== 
public class Oddity { 
 public static boolean isOdd(int i) { 
 return i % 2 == 1; 
 }  
} 


Answer: When passing negative numbers to the function, it will fail. This happens due to the way Jav treats
remainder operation works: it implies that when the remainder operation returns a result (non-zero values), the same sign as its left operand will be applied to it.

So basically if you are using the expression "i % 2 == 1" and pass a negative value (say -1), the remainder of the operation will be "minus 1", 
so the expression "-1 == 1" will return false.

A fix for this problem would be to change the expression to "i % 2 != 0;"

