package cocus;

import java.util.Arrays;
import java.util.List;

public class Question_1 {

	public static void main(String[] args) {
		List<Integer> items = Arrays.asList(  -2, -1, 0, 1, 2 );
        
		System.out.print("Original method:\n");
        for(Integer item : items){
            System.out.print("is " + item + " odd? " + isOddORIGINAL(item) + "\n");
        }
        
        System.out.print("\nFixed method:\n");
        
        for(Integer item : items){
            System.out.print("is " + item + " odd? " + isOddFIXED(item) + "\n");
        }

	}

	 public static boolean isOddORIGINAL(int i) { 
	        return i % 2 == 1; 
	 }
	 
	 public static boolean isOddFIXED(int i) { 
	        return i % 2 != 0; 
	 }
}
